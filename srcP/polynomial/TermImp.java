package polynomial;

import java.util.StringTokenizer;

import list.ArrayList;
import list.List;


public class TermImp implements Term {
	private double coefficient;
	private int exponent;

	public TermImp(double c, int e) {
		coefficient = c;
		exponent = e;
	}
	@Override
	public double getCoefficient() {
		
		return this.coefficient;
	}

	@Override
	public int getExponent() {
		return this.exponent;
	}

	@Override
	public double evaluate(double x) {
		return this.coefficient*Math.pow(x, this.exponent);
	}
	
	@Override
	public String toString() {
		if(this.exponent == 0)
			return String.format("%.2f", this.coefficient);
		else if(this.exponent == 1)
			return String.format("%.2fx", this.coefficient);
		else
			return String.format("%.2fx^%d", this.coefficient, this.exponent);
	}
	
	public static Term stringConverter(String str) {
		String tmp = new String (str);
		TermImp answer = null;
		if(tmp.contains("x^")) {
			StringTokenizer strToken = new StringTokenizer(tmp, "x^");
			List<String> list = new ArrayList<String>();
			while(strToken.hasMoreElements()) {
				list.add((String) strToken.nextElement());
			}
			if (list.size() == 0) throw new IllegalArgumentException("Argument string is formatted illegally.");
			else if(list.size() == 1) {
				Integer exp = Integer.parseInt(list.get(0));
				answer = new TermImp(1, exp);
			}
			else {
				Double coefficient = Double.parseDouble(list.get(0));
				Integer exponent = Integer.parseInt(list.get(1));
				answer = new TermImp(coefficient, exponent);
			}
		}
		else if(tmp.contains("x")) {
			StringTokenizer strToken = new StringTokenizer(tmp, "x");
			List<String> list = new ArrayList<String>();
			while(strToken.hasMoreElements()) {
				list.add((String) strToken.nextElement());
			}
			if (list.size() == 0) 
				answer = new TermImp(1.0, 1);
			else  {
				Double coefficient = Double.parseDouble(list.get(0));
				answer = new TermImp(coefficient, 1);
			}
			
		}
		else {
				answer = new TermImp(Double.parseDouble(tmp), 0);
			}
			
		return answer;
		
	}

}
