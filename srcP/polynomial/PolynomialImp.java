package polynomial;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

public class PolynomialImp implements Polynomial {

	private list.List<Term> terms;

	public PolynomialImp(String str) {
		this.terms = TermListFactory.newListFactory().newInstance();
		stringConverter(str);
	}

	@Override
	public Iterator<Term> iterator() {
		return this.terms.iterator();
	}

	@Override
	public Polynomial add(Polynomial P2) {
		PolynomialImp answer = new PolynomialImp("");
		for (Term trm : this.terms) {
			boolean find = false;
			for (Term trm2 : P2) {
				if(trm.getExponent() == trm2.getExponent()) {
					find = true;
					TermImp result = new TermImp(trm.getCoefficient() + trm2.getCoefficient(), trm.getExponent());
					answer.organizeTerm(result);
				}
			}
			if(!find) {
				TermImp result = new TermImp(trm.getCoefficient(), trm.getExponent());
				answer.organizeTerm(result);

			}
		}
		for(Term trm: P2) {
			boolean nFound = true;
			for (Term trm1 : terms) {
				if(trm.getExponent() == trm1.getExponent())
					nFound = false;

			}
			if(nFound) {
				TermImp result = new TermImp(trm.getCoefficient(),trm.getExponent());
				answer.organizeTerm(result);
			}
		}


		return answer;
	}

	@Override
	public Polynomial subtract(Polynomial P2) {
		return this.add(P2.multiply(-1));
	}

	@Override
	public Polynomial multiply(Polynomial P2) {
		List<PolynomialImp> positions =  new ArrayList<PolynomialImp>();
		for (Term trm : this.terms) {
			PolynomialImp answer = new PolynomialImp("");
			for (Term trm2 : P2) {
				TermImp result = new TermImp(trm.getCoefficient()*trm2.getCoefficient(), trm.getExponent()+trm2.getExponent());
				answer.organizeTerm(result);
			}
			positions.add((PolynomialImp) answer);

		}
		for (int i = 0; i < positions.size()-1; i++) {
			positions.set(0, (PolynomialImp)  positions.get(0).add(positions.get(i+1)));
		}
		return (Polynomial) positions.get(0);
	}

	@Override
	public Polynomial multiply(double c) {
		PolynomialImp answer = new PolynomialImp("");

		if(c == 0) {
			answer.organizeTerm(new TermImp(0,0));
			return answer;
		}else {
			for (Term trm : this.terms) {
				TermImp result = new TermImp(trm.getCoefficient()*c, trm.getExponent());
				answer.organizeTerm(result);
			}
		}
		return answer;
	}

	@Override
	public Polynomial derivative() {
		PolynomialImp answer = new PolynomialImp("");
		for (Term trm : this.terms) {
			if(trm.getExponent() != 0) {
				TermImp result = new TermImp(trm.getCoefficient()*trm.getExponent(), trm.getExponent()-1);
				answer.organizeTerm(result);
			}
		}
		return answer;
	}

	@Override
	public Polynomial indefiniteIntegral() {
		PolynomialImp answer = new PolynomialImp("");
		for (Term trm : this.terms) {
			TermImp result = new TermImp(trm.getCoefficient()/(trm.getExponent()+1), trm.getExponent()+1);
			answer.organizeTerm(result);
		}
		answer.organizeTerm(new TermImp(1,0));
		return answer;
	}

	@Override
	public double definiteIntegral(double a, double b) {
		Polynomial integral = this.indefiniteIntegral();
		return integral.evaluate(b) - integral.evaluate(a);
	}

	@Override
	public int degree() {
		return ((list.List<Term>) this.terms).first().getExponent();
	}

	@Override
	public double evaluate(double x) {
		double answer = 0;
		for (Term trm : terms) {
			answer += trm.evaluate(x);
		}
		return answer;
	}

	@Override
	public boolean equals(Polynomial P) {
		return this.toString().equals(P.toString());
	}
	
	private void organizeTerm(Term trm) {
		if(trm.getExponent() != 0) {
			if(this.terms.isEmpty())
				terms.add(trm);
			else {
				boolean notAdded = true;
				for (int i = 0; i < terms.size(); i++) {
					if(trm.getExponent() > this.terms.get(i).getExponent()) {
						this.terms.add(i, trm);
						notAdded = false;
						break;
					}
					if(trm.getCoefficient() == 0)
						this.terms.remove(i);
				}
				if(notAdded) {
					this.terms.add(trm);
				}
			}
		}
		
		else if (trm.getExponent() == 0) {
			this.terms.add(trm);
		}
		else {
			if(this.terms.isEmpty() && trm.getExponent() ==0)
				this.terms.add(trm);			
		}
	}


	private void stringConverter(String string) {
		StringTokenizer stringToken = new StringTokenizer(string, "+");
		String str = null;
		Term nextTerm = null;

		this.terms.clear();
		while(stringToken.hasMoreElements()) {
			str = (String) stringToken.nextElement();
			nextTerm = TermImp.stringConverter(str);
			this.organizeTerm(nextTerm);
		}
	}
	public String toString() {
		String result = "";
		if(terms.isEmpty())
			return "0.00";
		for (Term term : terms) {
			result += term + "+";
		}
		
		return result.substring(0, result.length()-1);

	}

}
