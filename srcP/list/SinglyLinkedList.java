package list;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;

public class SinglyLinkedList<E> implements List<E> {

	private static class ListIterator<E> implements Iterator<E>{

		private Node<E> current;

		public ListIterator(SinglyLinkedList<E> list) {
			this.current = list.header.getNext();
		}

		@Override
		public boolean hasNext() {
			return this.current != null;
		}

		@Override
		public E next() {

			if(this.hasNext()) {
				E result = current.getElement();
				current = current.getNext();
				return result;
			}
			else
				throw new NoSuchElementException();
		}

	}

	private static class Node<E>{

		private E element;
		private Node<E> next;

		public Node() {
			this.element = null;
			this.next = null;
		}

		public Node(E e, Node<E> n) {
			this.element = e;
			this.next = n;
		}

		public E getElement() {
			return this.element;
		}

		public void setElement(E e) {
			this.element = e;
		}

		public Node<E> getNext(){
			return this.next;
		}

		public void setNext(Node<E> n){
			this.next = n;
		}

	}

	private Node<E> header;
	private int size;

	public SinglyLinkedList() {
		this.header = new Node<E>();
		this.size = 0;
	}

	@Override
	public Iterator<E> iterator() {
		return new ListIterator<E>(this);
	}

	@Override
	public void add(E obj) {
		Node<E> temp = new Node<E>(obj, null);

		if(this.isEmpty())
			this.header.setNext(temp);
		else {

			Node<E> last = this.header;

			while(last.getNext() != null)
				last = last.getNext();

			last.setNext(temp);

		}

		this.size++;

	}

	@Override
	public void add(int index, E obj) throws IndexOutOfBoundsException {
		if(index < 0 || index > this.size()) throw new IndexOutOfBoundsException("Index Out Of Bounds.");

		Node<E> target = this.header;
		Node<E> temp = new Node<E>(obj, null);

		for(int a=0; a<index; a++)
			target = target.getNext();

		temp.setNext(target.getNext());
		target.setNext(temp);
		this.size++;

	}

	@Override
	public boolean remove(E obj) throws IllegalStateException {
		if(this.isEmpty()) throw new IllegalStateException("Illegal State");

		Node<E> target = this.header;

		while(target.getNext() != null) {

			if(target.getNext().getElement().equals(obj)) {

				Node<E> temp = target.getNext();
				target.setNext(temp.getNext());
				temp.setNext(null);
				this.size--;

				return true;

			}else

				target = target.getNext();

		}

		return false;

	}

	@Override
	public boolean remove(int index) throws IndexOutOfBoundsException {
		if(index < 0 || index > this.size()-1) throw new IndexOutOfBoundsException("Index Out Of Bounds");


		Node<E> target = this.header;

		if(index == 0) {

			this.header.setNext(target.getNext());
			target.setNext(null);
			this.size--;

		}
		else {
			for(int a=0; a<index; a++)
				target = target.getNext();

			Node<E> temp = target.getNext();
			target.setNext(temp.getNext());
			temp.setNext(null);
			this.size--;

		}

		return true;

	}

	@Override
	public int removeAll(E obj) {
		int counter=0;
		Node<E> target = this.header;

		while(target.getNext() != null) {

			if(target.getNext().getElement().equals(obj)) {

				Node<E> temp = target.getNext();
				target.setNext(temp.getNext());
				temp.setNext(null);
				this.size--;
				counter++;

			}else

				target = target.getNext();

		}

		return counter;

	}

	@Override
	public E get(int index) {
		Node<E> temp = this.header;

		for(int a=0; a<index; a++)
			temp = temp.getNext();

		return temp.getNext().getElement();

	}

	@Override
	public E set(int index, E obj) throws IndexOutOfBoundsException {
		if(index < 0 || index > this.size()) throw new IndexOutOfBoundsException("Index Out Of Bounds");

		Node<E> target = this.header;

		for(int a=0; a<index; a++)
			target = target.getNext();

		E result = target.getNext().getElement();
		target.getNext().setElement(obj);

		return result;

	}

	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {
		Node<E> temp = this.header;

		while(temp.getNext() != null)
			temp = temp.getNext();

		return temp.getElement();

	}

	@Override
	public int firstIndex(E obj) {
		Node<E> target = this.header.getNext();

		for(int a=0; a<this.size(); a++) {

			if(target.getElement().equals(obj))
				return a;
			else
				target = target.getNext();

		}

		return -1;

	}

	@Override
	public int lastIndex(E obj) {
		Node<E> target = this.header.getNext();
		int index=-1;

		for(int a=0; a<this.size(); a++) {

			if(target.getElement().equals(obj))
				index = a;

			target = target.getNext();

		}

		return index;

	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean isEmpty() {
		return this.size() == 0;
	}

	@Override
	public boolean contains(E obj) {
		Node<E> target = this.header.getNext();
		while(target.getNext() != null) {
			if(target.getElement().equals(obj))
				return true;
			target = target.getNext();

		}

		return false;

	}

	@Override
	public void clear() {
		while(!this.isEmpty())
			this.remove(0);

	}



}
