package list;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class ArrayList<E> implements List<E> {
	    private int CAPTOAR = 5;
	    private int INITCAP = 5;
	    private int MAXEMPTYPOS = 10;
	    private int size;
	    private E[] elementE;
	    
	    public ArrayList() {
	        this.elementE = (E[]) new Object[INITCAP]; 
	        this.size = 0;

	    }
	    private void changeCapacity(int change) { 
	        int newCapacity = elementE.length + change; 
	        E[] newElement = (E[]) new Object[newCapacity]; 
	        for (int i=0; i<size; i++) { 
	           newElement[i] = elementE[i]; 
	           elementE[i] = null; 
	        } 
	        elementE = newElement; 
	     }
	    private void moveDataOnePositionTR(int low, int sup) { 
			for (int pos = sup; pos >= low; pos--)
				elementE[pos+1] = elementE[pos]; 
		}

		private void moveDataOnePositionTL(int low, int sup) { 
			for (int pos = low; pos <= sup; pos++)
				elementE[pos-1] = elementE[pos]; 
		}


	    
	    private class ListIterator<E> implements Iterator<E>{
	    	private int current = 0;
	    	public ListIterator() {
	    		this.current = 0;
	    	}

			public boolean hasNext() {
				return this.current < size();
			}

			public E next() {
				if(this.hasNext()) {
					E result = null;
					result = (E) elementE[this.current++];
					return result;
				}
				else
					throw new NoSuchElementException();
					
			}
			
	    	
	    }
		public Iterator<E> iterator() {
			return new ListIterator<E>();
		}

		public void add(E obj) {
			if(this.size == elementE.length) {
	            changeCapacity(CAPTOAR);
	        }
	        
	        this.elementE[size++] = obj;
		}
		public void add(int index, E obj) {
				if (index<0 || index > size) {
					throw new IndexOutOfBoundsException("add: invalid index = " + index);
				}
				if (size == elementE.length) {
					changeCapacity(CAPTOAR);	
					}
				moveDataOnePositionTR(index, size-1);
				elementE[index] = obj;
				size++;
			}			
		
		public boolean remove(E obj) {
			if(obj == null) throw new IllegalArgumentException("Argument can not be null");
			int obtr = -1;
			for (int i = 0; i < this.size; i++) {
				if(elementE[i] == obj)
					obtr = i;		
			}
			if(obtr == -1)
				return false;
			else {
				for (int i = 0; i < this.size-1; i++) {
					elementE[i] = elementE[i++];
				}
				elementE[--size] = null;
				return true;
			}
		}
		
		public boolean remove(int index) {
			if (index<0 || index >= this.size) {
				throw new IndexOutOfBoundsException("remove: invalid index = " + index);
			}
			elementE[index] = null;
			moveDataOnePositionTL(index+1, this.size-1);
			size--;
			if((elementE.length - this.size) > MAXEMPTYPOS) {
	            this.changeCapacity(-CAPTOAR);
	        }
			return true;
		}
		public int removeAll(E obj) {
			int counter = 0;
			
			while(this.remove(obj)) 
					counter++;
			return counter;
		}
		public E get(int index) {
			if (index<0 || index > size-1) {
				throw new IndexOutOfBoundsException("get: Invalid index = " + index);
			}
			else {
				return elementE[index];
			}
		}
		public E set(int index, E obj) {
			if (index<0 || index > size-1) {
				throw new IndexOutOfBoundsException("set: invalid index = " + index);
			}
				E tempVar = this.elementE[index];
				elementE[index] = obj;
				return tempVar;
		}
		public E first() {
			if(isEmpty()) return null;
			return elementE[0];
		}
		public E last() {
			if(isEmpty()) return null;
			return elementE[size-1];
		}
		public int firstIndex(E obj) {
			for (int i = 0; i < elementE.length; i++) {
				if(this.elementE[i].equals(obj))
					return i;
			}
			return -1;
		}
		public int lastIndex(E obj) {
			for (int i = size-1; i >= 0; --i) {
				if(this.elementE[i].equals(obj))
					return i;
			}
			return -1;
		}
		public int size() {
			return this.size;
		}
		public boolean isEmpty() {
			return size == 0;
		}
		public boolean contains(E obj) {
			return firstIndex(obj) >= 0;
		}
		public void clear() {
			for(int i = 0; i < elementE.length; i++)
				elementE[i] = null;
			this.size = 0;
		}

}

